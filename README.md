**Instalacja**: `npm install git+ssh://git@bitbucket.org:cubegroup/bigquery.git`


**Inicjalizacja**: `const BigQueryCG = require('bigquery-cg')(credentials={})`;


**Dostępne metody**:

1. `getDatasetTable(datasetId, tableId, schema, callback)`
2. `removeDuplicates(query, rows, callback)`
3. `insertRows(table, rows, callback)`
4. `callQuery(options, callback)`
5. `deleteTable(table, callback)`
6. `copyTable(sourceTable, destinationTable, metadata, callback)`
7. `getSchema(data, types, dateField)`
8. `updateSchema(table, items, callback)`
9. `checkTableStructure(table, newSchema, callback)`
10. `getData(data, params, callback, previousExists)`
11. `getUniqueItems(table, resultData, params, callback)`
12. `getDatasetTableAndInsertUnique(datasetId, tableId, uniquePropBQ, data, callback, schema)`
13. `refreshTable(datasetName, tableName, declaredSchema, data, callback)`