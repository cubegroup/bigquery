const { BigQuery } = require('@google-cloud/bigquery');


const BigQueryCG = (credentials) => {
  const bigquery = new BigQuery(credentials);

  const getDataset = (datasetId, callback) => {
    const dataset = bigquery.dataset(datasetId);
    dataset.get().then(() => {
      callback(dataset);
    }).catch(() => {
      bigquery
        .createDataset(datasetId, {
          location: 'EU',
        }).then((results) => {
        callback(results[0]);
      })
        .catch((err) => {
          console.error(err);
          callback();
        });
    });
  };

  const getTable = (dataset, tableId, schema, callback) => {
    const table = dataset.table(tableId);
    table.get().then(() => {
      callback(table);
    }).catch(() => {
      let options;
      if (schema.schema) {
        options = schema;
      } else {
        options = { schema };
      }
      dataset.createTable(tableId, options)
        .then((results) => {
          callback(results[0]);
        })
        .catch((err) => {
          if (err && !err.errors) {
            console.error(err);
          } else if (err && err.errors && err.errors[0] && err.errors[0].reason !== 'notFound') {
            console.error(err);
          }
          callback();
        });
    });
  };

  const equals = function (x, y) {
    if (x === y) return true;
    // if both x and y are null or undefined and exactly the same
    if (!(x instanceof Object) || !(y instanceof Object)) return false;
    // if they are not strictly equal, they both need to be Objects
    if (x.constructor !== y.constructor) return false;
    // they must have the exact same prototype chain, the closest we can do is
    // test there constructor.
    for (var p in x) {
      if (!x.hasOwnProperty(p)) {
        continue;
      }
      // other properties were tested using x.constructor === y.constructor
      if (!y.hasOwnProperty(p)) {
        return false;
      }
      // allows to compare x[ p ] and y[ p ] when set to undefined
      if (x[p] === y[p]) {
        continue;
      }
      // if they have the same strict value or identity then they are equal
      if (typeof (x[p]) !== 'object') {
        return false;
      }
      // Numbers, Strings, Functions, Booleans must be strictly equal
      if (!equals(x[p], y[p])) {
        return false;
      }
      // Objects and Arrays must be tested recursively
    }
    for (p in y) {
      if (y.hasOwnProperty(p) && !x.hasOwnProperty(p)) {
        return false;
      }
      // allows x[ p ] to be set to undefined
    }
    return true;
  };

  return {
    getDatasetTable: function(datasetId, tableId, schema, callback) {
      getDataset(datasetId, (dataset) => {
        getTable(dataset, tableId, schema, callback);
      });
    },
    removeDuplicates: function(query, rows, callback) {
      bigquery
        .query({
          query,
          useLegacySql: false,
        })
        .then((results) => {
          const exists = results[0].map((row) => {
            // @TODO tu trzeba będzie w przyszłości bardziej uniwersalną zasadę napisać
            row.date = row.date.value.replace('T', ' ');
            return row;
          });
          callback(rows.filter((row) => {
            const _rows = exists.filter(row2 => equals(row2, row));
            return !(_rows.length);
          }));
        });
    },
    insertRows: function(table, rows, callback) {
      if (table && rows.length) {
        table.insert(rows.filter((row, i) => i < 1000))
          .then(() => {
            console.log(`Inserted ${rows.length > 1000 ? 1000 : rows.length} rows`);
            if (rows.length > 1000) {
              insertRows(table, rows.filter((row, i) => i >= 1000), callback);
            } else {
              callback();
            }
          })
          .catch((err) => {
            if (err && err.name === 'PartialFailureError') {
              if (err.errors && err.errors.length > 0) {
                console.log('Insert errors:');
                err.errors.forEach(err => console.error(err));
              }
            } else {
              console.error('ERROR:', err);
            }
            callback();
          });
      } else {
        callback();
      }
    },
    callQuery: function(options, callback) {
      bigquery
        .query(options)
        .catch((error) => {
          if (error) {
            console.error(options, error);
          }
        })
        .then((results) => {
          callback(results);
        });
    },
    deleteTable: function(table, callback) {
      if (table) {
        table.delete()
          .then(() => {
            console.log(`Tabela ${table.id} usunięta`);
            callback();
          })
          .catch((err) => {
            console.error('ERROR:', err);
            callback();
          });
      } else {
        callback();
      }
    },
    copyTable: function(sourceTable, destinationTable, metadata, callback) {
      sourceTable.copy(destinationTable, metadata || {})
        .then(() => {
          console.log(`Tabela ${sourceTable.id} przekopiowana do ${destinationTable.id}`);
          callback();
        })
        .catch((err) => {
          console.error('ERROR:', err);
          callback();
        });
    },
    getSchema: function(data, types, dateField) {
      let columns;
      const fieldsObject = {};
      data.forEach((item) => {
        columns = Object.keys(item);
        columns.map((column) => {
          if (types && types[column]) {
            fieldsObject[column] = types[column];
          } else if (item[column] === true || item[column] === false) {
            fieldsObject[column] = 'BOOLEAN';
          } else if (typeof item[column] === 'number' && item[column] % 1 !== 0) {
            fieldsObject[column] = 'FLOAT';
          } else if (typeof item[column] === 'number' && !fieldsObject[column]) {
            fieldsObject[column] = 'INTEGER';
          } else if (column === dateField) {
            fieldsObject[column] = 'DATE';
          } else if (!fieldsObject[column]) {
            fieldsObject[column] = 'STRING';
          }
        });
      });
      columns = Object.keys(fieldsObject);
      const schema = {
        fields: columns.map(name => ({
          name, type: fieldsObject[name],
        })),
      };
      if (dateField) {
        return {
          timePartitioning: {
            field: dateField,
            type: 'DAY',
          },
          schema,
        };
      }
      return schema;
    },
    /**
     * Funkcja buduje obiekt opisujący schemat tabeli, który można wykorzystać przy tworzeniu tabeli w BigQuery.
     * Schemat jest budowany na podstawie dancyh zapisanych w tablice items.
     *
     * @async
     * @param {Object} table - Objekt tabeli zwrócony przez API BigQuery.
     * @param {Array} items - Tabela obiektów, które będą wstawione do tabeli.
     * @param {Function} callback - Dalsze kroki.
     */
    updateSchema: function(table, items, callback) {
      const newSchema = getSchema(items);
      table.getMetadata().then((data) => {
        const { schema } = data[0];
        if (!schema || schema.fields.length < newSchema.fields.length) {
          console.log('zmiana schematu!');
          const metadata = {
            schema: newSchema,
          };

          table.setMetadata(metadata).then(() => {
            callback();
          }).catch((e) => {
            console.warn(e);
            callback();
          });
        } else {
          callback();
        }
      }).catch((e) => {
        console.warn(e);
        callback();
      });
    },
    /**
     * Funkcja sprawdza obecny schemat tabeli i jeśli rozpozna zmianę, to dostosowuje
     * go do nowego schematu. W BigQuery można tylko zmieniać schemat poprzez dodanie pól,
     * dlatego ważne jest by nowy schemat zawierał wszystkie pola z poprzedniego schematu.
     *
     * @async
     * @param {Object} table - Tabela (wynik funkcji getTable).
     * @param {Object} newSchema - Nowy schemat bazy (zalecany wynik funkcji getSchema).
     * @param {Function} callback - Dalsze kroki.
     */
    checkTableStructure: function(table, newSchema, callback) {
      // pobieramy aktualny schemat tabeli
      if (table) {
        table.getMetadata().then((tableData) => {
          const { schema } = tableData[0];
          // jeśli aktualny scemat ma mniej pół niż nowy, to go zmieniamy
          // schemat można tylko zmieniać poprzez dodanie nowych pól, dlatego ten warunek
          // jest wystarczający
          if (schema.fields.length < newSchema.fields.length) {
            console.log('zmiana schematu!');
            const metadata = {
              schema: newSchema,
            };

            // ustawienie nowego scheatu
            table.setMetadata(metadata).then(() => {
              // dalsze kroki
              callback();
            }).catch((e) => {
              // przechwycenie błędu
              console.error(e);
              // dalsze kroki
              callback();
            });
          } else {
            // dalsze kroki
            callback();
          }
        }).catch((e) => {
          // przechwycenie błędu
          console.error(e);
          // dalsze kroki
          callback();
        });
      } else {
        callback();
      }
    },
    /**
     *
     * @param data
     * @param params
     * @param callback
     * @param previousExists
     */
    getData: function(data, params, callback, previousExists) {
      const quoteChar = typeof data[0] === 'number' ? '' : '"';
      const options = {
        query: `SELECT ${params.uniquePropBQ} FROM ${params.datasetId}.${params.tableId} WHERE ${params.uniquePropBQ} IN (${quoteChar}${data.filter((row, i) => i < 5000).join(`${quoteChar},${quoteChar}`)}${quoteChar})`,
        useLegacySql: false, // Use standard SQL syntax for queries.
      };
      // Runs the query
      this.callQuery(options, (results) => {
        const clientsId = previousExists || {};
        if (results && results[0]) {
          results[0].forEach((row) => {
            clientsId[row[params.uniquePropBQ]] = true;
          });
        }
        if (data.length > 5000) {
          getData(data.filter((row, i) => i >= 5000), params, callback, clientsId);
        } else {
          callback(clientsId);
        }
      });
    },
    /**
     *
     * @param table
     * @param resultData
     * @param params
     * @param callback
     */
    getUniqueItems: function(table, resultData, params, callback) {
      const ids = resultData.map(row => row[params.uniquePropBQ]);
      this.getData(ids, params, (exists) => {
        const rows = resultData.filter(row => !exists[row[params.uniquePropBQ]]);
        if (rows.length) {
          this.insertRows(table, rows, () => {
            if (callback) {
              callback();
            }
          });
        } else if (callback) {
          callback();
        }
      });
    },
    /**
     * Funkcja obsługująca najpopularniejsze flow pracy z danymi, pobiera tabele lub ją tworzy
     * na bazie schematu odczytanego z danych, jeśli jest potrzeba to schemat jest aktualizowany
     * następnie sprawdza czy dane nie istnieją już w bazie i te, które są nowe dodaje do bazy.
     *
     * @param {string} datasetId - Nazwa datasetu.
     * @param {string} tableId - Nazwa tabeli.
     * @param {string} uniquePropBQ - Nazwa kolumny, w której przechowywany jest id.
     * @param {Array} data - Tablica obiektów do wstawienia.
     * @param {Function} callback
     */
    getDatasetTableAndInsertUnique: function(datasetId, tableId, uniquePropBQ, data, callback, schema) {
      const self = this;
      if (data.length) {
        const newSchema = self.getSchema(data, schema || {});
        // pobierz istaniejący dataset i tabelę, jeśli tabela nie istnieje to ją stworzy
        self.getDatasetTable(datasetId, tableId, newSchema, (table) => {
          // sprawdź czy schemat jest aktualny
          self.checkTableStructure(table, newSchema, () => {
            // weź unikatowe wartości i wstaw je do bazy
            self.getUniqueItems(table, data, {
              uniquePropBQ,
              tableId,
              datasetId,
            }, callback);
          });
        });
      } else {
        callback();
      }
    },
    /**
     * Usuwa tabelę z BigQuery i tworzy nową oraz wstawia do niej dane.
     *
     * @param {string} datasetName - Nazwa datasetu.
     * @param {string} tableName - Nazwa tabeli.
     * @param {Object} declaredSchema - Nowy schemat bazy (zalecany wynik funkcji getSchema).
     * @param {Array} data - Tablica obiektów do wstawienia.
     * @param callback
     */
    refreshTable: function(datasetName, tableName, declaredSchema, data, callback) {
      const self = this;
      const schema = declaredSchema || self.getSchema(data);
      if (datasetName) {
        self.getDatasetTable(datasetName, tableName, schema, (tableToDelete) => {
          self.deleteTable(tableToDelete, () => {
            self.getDatasetTable(datasetName, tableName, schema, (table) => {
              if (table) {
                self.insertRows(table, data, () => {
                  callback();
                });
              } else {
                console.log(`table ${datasetName}.${tableName} not exists`);
                setTimeout(() => {
                  self.refreshTable(datasetName, tableName, schema, data, callback);
                }, 1000);
              }
            });
          });
        });
      } else {
        callback();
      }
    }
  }
};

module.exports = BigQueryCG;